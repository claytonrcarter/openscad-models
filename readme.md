# OpenSCAD models

I have written most of these as I've learned about OpenSCAD. Some of them have been downloaded from Thingiverse.

- French Cleat Pen Holder - a simple pen cup for use on a french cleat; playing w/ linear and rotational extrusions
- [Customizable Box](https://www.thingiverse.com/thing:2954310) - parametric boxes for storing game cards (originally from Thingiverse, but includes modifications)
- Vacuum Seeder - various attempts I've made to model and print a handheld vacuum seeder for use the greenhouse. WIP: More or less works, but not "done".
- ShopVac Adapter - fitting for my shopvac for use w/ the vacuum seeder
- Reemay Clip - 1/2" EMT clips for reemay, modeled after [these](https://www.johnnyseeds.com/tools-supplies/supports-and-anchors/snap-clamps-for-1-2"-emt-9608.html)
- Cup Holder - a coffee cup insulator/holder for Doug's favorite cups
- Cuisinart Disc Holder - simple device to store food processor slicing discs on standing up
- Cuisinart Jig - A plug to allow the food processor to work w/o the large "pusher" in place. Makes it easier to slice/shred larger veggies.



