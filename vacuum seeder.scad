// TODO draw alignment grid on top of seeder

// constants; don't change these
mmPerIn = 25.4;
oneIn = 25.4; // mm
halfIn = oneIn/2;
quarterIn = oneIn/4;
oneCm = 10;

//
// Dimensions - change this to suit the size of your print bed
//
// 5.25 is my print bed (or 140mm)
seederBoxSize = 5.25 * oneIn; 

// 
// Grid Spacing - uncomment only one "group" of these variables
//
flatType = "128";              // For Landmark 128: (1.23" x 1.28" spacing)
rowSpacing = 1.25 * mmPerIn; 
rowOffset = rowSpacing/3;

//flatType = "72";             // For Landmark 72: (1.70" x 1.71" spacing)
//rowSpacing = 1.7 * mmPerIn; 
//rowOffset = rowSpacing/3;

//flatType = "blank";          // default settings
//rowSpacing = oneIn;
//rowOffset = 0;

//
// Holes/Grid
//
makeHoles = true;   // true = make holes; false = don't make holes
countersink = true; // true = countersunk holes; false = straight holes
holeSize = 2;       // hole size (diameter, in mm)

//
// More constants - probably best not to change these
// 
handleHeight = oneIn;
handleResolution = 360; // use 0 for drafting; 360 for final rendering

coneHeight = 4;
nozzleHeight = 10;
nozzleDiameter = 10; // mm

nozzleRadius = nozzleDiameter/2;
nozzleBaseThickness = 1; // mm
handleWallThickness = .5; // mm
wallThickness = 1; // mm

rowCount = floor(seederBoxSize/rowSpacing);
outerDimension = ((rowCount - 1) * rowSpacing) + nozzleDiameter;
firstRow = rowSpacing - rowOffset;
lastRow = (rowCount * rowSpacing) - rowOffset;


difference() {
    // create the box and the inlaid grid
    difference() {
        union() {
            nozzleBodies();
            supplyTubes();
//                seederBox();
//                seederBoxDividers();
            
            // add solid handles
            uprightHandle(firstRow);
            uprightHandle(lastRow);
            handle();
            
//            label();

        }
        union() {
            nozzleBodyHollows();
            supplyTubeHollows();

            hollowHandle();
            hollowUprightHandle(firstRow);
            hollowUprightHandle(lastRow);
            
            // DEBUG: horizontal slice
//            translate([0, 0, nozzleHeight * 1.25])
//                cube(size = [125, 125, nozzleHeight]);
            
            // DEBUG: x slice
//                cube(size = [125, 125/2, 125]);

            // DEBUG: y slice
//                cube(size = [(1 * rowSpacing) - rowOffset, 125, 125]);
        }

    }
}

// 
module nozzleBodies() {    

    for (i = [1 : rowCount]) {
        for (j = [1 : rowCount]) {
            x = (i * rowSpacing) - rowOffset;
            y = (j * rowSpacing) - rowOffset;

            // make the cone
            // r = radius, or r1 = bottom radius, r2 = top radius
            translate([x, y, 0])
                cylinder(h=coneHeight, r1=holeSize+.5, r2=5);    

            // make the main body
            translate([x, y, coneHeight])
                cylinder(h=5, r=5);    

        }
    }
}
        
// 
module nozzleBodyHollows() {    
    for (i = [1 : rowCount]) {
        for (j = [1 : rowCount]) {
            x = (i * rowSpacing) - rowOffset;
            y = (j * rowSpacing) - rowOffset;
            // make the actual hole
            translate([x, y, 0])
                cylinder(h=nozzleBaseThickness + nozzleHeight, r=holeSize/2);
            
            // make the countersinks
            if (countersink) {
                translate([x, y, 0])
                    //  r1 = bottom radius, r2 = top radius
                    cylinder(h=nozzleBaseThickness, r1=holeSize, r2=0, $fa=1);
            }
            
            // clear out the main nozzle body
            translate([x, y, nozzleBaseThickness])
                cylinder(h=coneHeight, r1=holeSize+.5-wallThickness, r2=5-wallThickness);    
            translate([x, y, coneHeight+nozzleBaseThickness-1])
                cylinder(h=nozzleHeight/2, r=5-wallThickness);
        }
    }
}

module supplyTubes() {
    w = nozzleDiameter;
    l = outerDimension;
    for (i = [1 : rowCount]) {
        l = outerDimension - nozzleDiameter;
        x = (i * rowSpacing) - rowOffset;
        y = l/2 + rowSpacing - rowOffset;
        z = nozzleHeight * 1.125;
        
        translate([x, y, z])
        rotate([0, 45, 0])
            cube(size = [w, l, w], center = true);
    }
    
    x = l/2 + rowSpacing - rowOffset - 5;
    y1 = rowSpacing - rowOffset;
    y2 = (rowCount * rowSpacing) - rowOffset;
    z = nozzleHeight * 1.125;
    
    translate([x, y1, z])
    rotate([45, 0, 0])
        cube(size = [l + 5, w, w], center = true);
    
    translate([x, y2, z])
    rotate([45, 0, 0])
        cube(size = [l + 5, w, w], center = true);
}

module supplyTubeHollows() {
    w = nozzleDiameter - 2 * wallThickness;
    l = outerDimension - 2 * wallThickness;
    for (i = [1 : rowCount]) {
        l = outerDimension - nozzleDiameter;
        x = (i * rowSpacing) - rowOffset;
        y = l/2 + rowSpacing - rowOffset;
        z = nozzleHeight * 1.125;
        
        translate([x, y + wallThickness, z])
        rotate([0, 45, 0])
            cube(size = [w, l, w], center = true);
    }
    
    x = l/2 + rowSpacing - rowOffset - 5;
    y1 = rowSpacing - rowOffset;
    y2 = (rowCount * rowSpacing) - rowOffset;
    z = nozzleHeight * 1.125;
    
    translate([x + wallThickness, y1, z])
    rotate([45, 0, 0])
        cube(size = [l, w, w], center = true);
    
    translate([x + wallThickness, y2, z])
    rotate([45, 0, 0])
        cube(size = [l, w, w], center = true);
}


//// support dividers inside the box
//module seederBoxDividers() {
//    dividerSpacing = 5; // mm
//    dividerCount = floor(seederBoxSize / dividerSpacing);
//    
//    for(n = [1 : dividerCount])
//        translate([n*dividerSpacing, dividerSpacing, 0]) 
//        rotate([0,0,90])
//            cube(size = [seederBoxSize-10,.5,10], center = false);
//}

// an individual upright handle
module uprightHandle(y) {
    translate([firstRow + (lastRow-firstRow)/2, y, nozzleHeight])
        cylinder(h=handleHeight, r=quarterIn/2);
}

// hollow portion of an upright handle
module hollowUprightHandle(y) {
    translate([firstRow + (lastRow-firstRow)/2, y, nozzleHeight])
    translate([0,0,-1])
        cylinder(h=handleHeight + 2, r=(quarterIn/2) - handleWallThickness);
}

// main (horizontal) handle
module handle() {
    x = firstRow + (lastRow-firstRow)/2;
    y = rowOffset;
    z = handleHeight+nozzleHeight;
    
    l = lastRow-firstRow+nozzleDiameter*2;
    
    translate([x, y, z])
    rotate([-90,0,0])
        cylinder(h=l, r=quarterIn/2, $fn=handleResolution);
}

// hollow portion of the horizontal handle
module hollowHandle() {
    translate([firstRow + (lastRow-firstRow)/2, 0, handleHeight+nozzleHeight])
    rotate([-90,0,0])
    translate([0,0,-1])
       cylinder(h=seederBoxSize + 2, r=(quarterIn/2) - handleWallThickness);  
}

module label() {
    if (flatType != "" && flatType != "blank") {
        translate([firstRow, firstRow-7, nozzleHeight+2])    
        rotate([45, 0, 0])
        text(str(flatType, " ", "pelleted"), size=8);
//        translate([firstRow, firstRow+1+15, nozzleHeight+7.5+2])    
//            text("pellet");
        
    }
}