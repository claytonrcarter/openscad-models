
length=40;

// The prefab clips measure 21.75mm from outside to outside
// and have a wall thickness of 1.94. The opening, from edge
// to edge is 15.05 mm.
outerR=21.75/2;
innerR=outerR-1.94;
openingWidth=15.05;

difference() {
    // outer surface
    cylinder(h=length, r1=outerR, r2=outerR, $fn=360);  
    
    union() {
        
        // inner surface
        cylinder(h=length, r1=innerR, r2=innerR, $fn=360);  

        // rough opening
        translate([10, 0, length/2])
            cube(size = [openingWidth, openingWidth, length], center = true);

        // crude chamfer for the opening
        translate([8.5, 0, length/2])
        rotate([0, 0, 45])
            cube(size = [openingWidth, openingWidth, length], center = true);


    }

}
