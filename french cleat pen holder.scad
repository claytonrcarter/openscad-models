mmPerIn = 25.4;
oneIn = mmPerIn/1;
halfIn = mmPerIn/2;
quarterIn = mmPerIn/4;
threeQuarterIn = mmPerIn*.75;
oneCm = 10;

cleatHeight=oneIn*1.5;
cleatDepth=threeQuarterIn-1; //-1 to give some tolerance

cupWidth=oneIn*2;
cupDepth=oneIn;
cupHeight=oneIn*3;
cupWallThickness=1;

//*****************************************************************************/
// Draw the Model
//*****************************************************************************/

cleat();
cup();
cupBottom();

//*****************************************************************************/
// Module Definitions
//*****************************************************************************/

module cup() {
    linear_extrude(height=cupHeight)
    union() {       
        cupWall();
        translate([0,cupWidth-cupWallThickness,0]) cupWall();
        cupFront();
        cupBack();
    }
}


module cupWall() {
    translate([cleatDepth,0,0])
    polygon([
        [0,0],
        [0,cupWallThickness],
        [0+cupDepth,cupWallThickness],
        [0+cupDepth,0]
    ]);
}

function arccoords(startangle, step, endangle, xy_size, xoffset, yoffset)
    = ((step > 0 && startangle > endangle) || (step < 0 && startangle < endangle))
        ? []
        : concat([[xy_size * sin(startangle)+xoffset, xy_size * cos(startangle)+yoffset]],
                 arccoords(startangle+step, step, endangle, xy_size, xoffset, yoffset)); 

module cupFront() {
    translate([cleatDepth+cupDepth,cupWidth/2,.5])
    arcWall();
}

// Draw a thin wall along an arc; ie an outline or circumference of a circle
module arcWall() {
    polygon(concat(
        arccoords(0, 1, 180, cupWidth/2, 0, 0),
        arccoords(0, 1, 180, (cupWidth-(2*cupWallThickness))/2, 0, 0)
    ));
}

module cupBack() {
    translate([cleatDepth,0,0])
    polygon([
        [0,0],
        [0,cupWidth],
        [0+cupWallThickness,cupWidth],
        [0+cupWallThickness,0],
    ]);
}

module cupBottom() {
    union() {

        translate([cleatDepth,0,cupHeight])
        rotate([0,-90,0])
        translate([0,cupWidth/2,-1])
        linear_extrude(height=cupWallThickness)
        polygon(arccoords(0, 1, 180, cupWidth/2, 0, 0));
    
        translate([cleatDepth+cupDepth,0,cupHeight])
        rotate([0,-90,0])
        translate([0,cupWidth/2,-.5])
        linear_extrude(height=cupDepth)
        arcWall();
    
        translate([cleatDepth+cupDepth,0,cupHeight])
        translate([0,cupWidth/2,0])
        rotate([90,0,0])
        rotate_extrude(angle = 90,$fn = 100)
        arcWall();
    }
}

module cleat() {
    translate([0,0,cleatHeight])
    rotate([-90,0,0])
    linear_extrude(height=cupWidth)
    polygon([
        [0,0],
        [0,cleatHeight],
        [cleatDepth,cleatHeight],
        [cleatDepth,cleatDepth]
    ]);
}