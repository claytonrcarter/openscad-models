 
discThickness = 20;
boxLength = 139;
boxWidth = discThickness/2 + discThickness * 4;
slotLength = 158;
 
module leg() {
  cube([10, 10, 70]);
}
 
leg();

translate([boxLength - 10, 0, 0])
  leg();

translate([0, boxWidth - 10, 0])
  leg();

translate([boxLength - 10, boxWidth - 10, 0])
  leg();


difference() {
    
  cube([boxLength, boxWidth, 10]);
    
  union() {
      
      for(i = [0:3]) {
          
      translate([10, discThickness/2 + discThickness * i, 0])
          cube([slotLength, 10, 10]);

      }
  }
    
}