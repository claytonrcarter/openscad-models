// TODO draw alignment grid on top of seeder

// constants; don't change these
mmPerIn = 25.4;
oneIn = 25.4; // mm
halfIn = oneIn/2;
quarterIn = oneIn/4;
oneCm = 10;

//
// Dimensions - change this to suit the size of your print bed
//
seederBoxSize = 5.25 * oneIn;

// 
// Grid Spacing - uncomment only one "group" of these variables
//
flatType = "128";              // For Landmark 128: (1.23" x 1.28" spacing)
gridSpacing = 1.25 * mmPerIn; 
gridOffset = gridSpacing/3;

//flatType = "72";             // For Landmark 72: (1.70" x 1.71" spacing)
//gridSpacing = 1.7 * mmPerIn; 
//gridOffset = gridSpacing/2;

//flatType = "blank";          // default settings
//gridSpacing = oneIn;
//gridOffset = 0;

//
// Holes/Grid
//
makeHoles = true;   // true = make holes; false = don't make holes
countersink = true; // true = countersunk holes; false = straight holes
holeSize = 2;       // hole size (diameter, in mm)
makeGrid = false;   // true = draw an inlaid grid; false = no grid

//
// More constants - probably best not to change these
// 
seederBoxHeight = 10;
handleHeight = oneIn;
seederBaseThickness = 1; // mm
seederWallThickness = .5; // mm
handleWallThickness = 1; // mm

difference() {
    // create the box with solid handles
    union() {
        // create the box and the inlaid grid
        difference() {
            union() {
                seederBox();
                seederBoxDividers();
            }
            if (makeGrid) grid();
            if (makeHoles) holes();
        }


        // add solid handles
        uprightHandle(seederBoxSize - oneIn);
        uprightHandle(oneIn);
        handle();
        
        label();
    }
    // remove the hollow portion of the handles
    union() {
        hollowHandle();
        hollowUprightHandle(seederBoxSize - oneIn);
        hollowUprightHandle(oneIn);
    }
}

// main box that forms the seeder
module seederBox() {
    // bottom; 1 mm thick
    cube(size = [seederBoxSize, seederBoxSize, seederBaseThickness], center = false);

    // top
    translate([0, 0, seederBoxHeight])
        cube(size = [seederBoxSize, seederBoxSize, seederWallThickness], center = false);

    // front
    cube(size = [seederBoxSize, seederWallThickness, seederBoxHeight], center = false);

    // back
    translate([0, seederBoxSize - seederWallThickness/2, 0])
        cube(size = [seederBoxSize, seederWallThickness, seederBoxHeight], center = false);

    // left
    translate([seederWallThickness, 0, 0]) 
    rotate([0,0,90])
        cube(size = [seederBoxSize, seederWallThickness, seederBoxHeight], center = false);
    
    // right
    translate([seederBoxSize, 0, 0]) 
    rotate([0,0,90])
        cube(size = [seederBoxSize, seederWallThickness, seederBoxHeight], center = false);
}

// an inlaid grid on the bottom of the box
module grid() {
    gridCount = floor(seederBoxSize/gridSpacing);
    gridLineThickness = .25;

    for (n = [1 : gridCount]) {
        translate([0,(n * gridSpacing) - gridOffset,0])
            cube(size = [seederBoxSize, gridLineThickness, gridLineThickness], center = false);
        translate([(n * gridSpacing) - gridOffset,0,0]) 
        rotate([0,0,90])
            cube(size = [seederBoxSize, gridLineThickness, gridLineThickness], center = false);

    }
}

// an inlaid grid on the bottom of the box
module holes() {
    gridCount = floor(seederBoxSize/gridSpacing);
    
    for (i = [1 : gridCount]) {
        for (j = [1 : gridCount]) {
            x = (i * gridSpacing) - gridOffset;
            y = (j * gridSpacing) - gridOffset;
            // make the actual hole
            translate([x, y, 0])
                cylinder(h=seederBaseThickness + .5, r=holeSize/2);
            
            // make the countersinks
            if (countersink) {
                translate([x, y, 0])
                    //  r1 = bottom radius, r2 = top radius
                    cylinder(h=seederBaseThickness, r1=holeSize, r2=0);
            }
            
            // make sure the in-box dividers are cleared out of the way
            translate([x, y, seederBaseThickness])
                cylinder(h=2, r=holeSize);
        }
    }
}

// support dividers inside the box
module seederBoxDividers() {
    dividerSpacing = 5; // mm
    dividerCount = floor(seederBoxSize / dividerSpacing);
    
    for(n = [1 : dividerCount])
        translate([n*dividerSpacing, dividerSpacing, 0]) 
        rotate([0,0,90])
            cube(size = [seederBoxSize-10,.5,10], center = false);
}

// an individual upright handle
module uprightHandle(y) {
    translate([seederBoxSize/2, y, seederBoxHeight])
        cylinder(h=handleHeight, r=quarterIn/2);
}

// hollow portion of an upright handle
module hollowUprightHandle(y) {
    translate([seederBoxSize/2, y, seederBoxHeight])
    translate([0,0,-1])
        cylinder(h=handleHeight + 2, r=(quarterIn/2) - handleWallThickness);
}

// main (horizontal) handle
module handle() {
    translate([seederBoxSize/2, 0, handleHeight+seederBoxHeight])
    rotate([-90,0,0])
        cylinder(h=seederBoxSize, r=quarterIn/2);
}

// hollow portion of the horizontal handle
module hollowHandle() {
    translate([seederBoxSize/2, 0, handleHeight+seederBoxHeight])
    rotate([-90,0,0])
    translate([0,0,-1])
       cylinder(h=seederBoxSize + 2, r=(quarterIn/2) - handleWallThickness);  
}

module label() {
    if (flatType != "" && flatType != "blank") {
        translate([1, 1, seederBoxHeight+1])    
            text(flatType);
    }
}