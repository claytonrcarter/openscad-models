
difference() {
    union() {
        cube([15,20,2]);

        translate([0,6,0])
            cube([2,8,50]);

        translate([0,8.5,0])
            cube([9,3,50]);
    }
    union() {
        
        translate([0,0,52])
        rotate([0,45,0])
            cube([20,20,10]);
        
        // maybe add another one here @ a steeper angle
        // would better match the built in 
        
    }
}