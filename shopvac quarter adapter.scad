
cylRes = 360;

bigDiam = 31.5+4;
bigRad=bigDiam/2;
bigHt=25;

mmPerIn = 25.4;
quarterIn = mmPerIn/4;

littleDiam = quarterIn;
littleRad=littleDiam/2;
littleHt=quarterIn*2;

difference() {
    union() {
        cylinder(h=bigHt, r=bigRad, $fn=cylRes);

        //    translate([0,0,bigHt-2])
        //    cylinder(h=2, r=bigDiam/2+1, $fn=360);
        
        translate([0,0,bigHt])        
            cylinder(h=bigHt, r1=bigRad, r2=littleRad); 
        
        translate([0,0,(bigHt-.25)*2])
            cylinder(h=littleHt, r=littleRad, $fn=cylRes);
        

    }
    
    union() {
        translate([0,0,-.125])        
            cylinder(h=bigHt+.25, r=bigRad-2);
                
        translate([0,0,bigHt-.125])        
            cylinder(h=bigHt+.25, r1=bigRad-2, r2=littleRad-1); 

        translate([0,0,bigHt*2-2])
            cylinder(h=littleHt+4, r=littleRad-.5, $fn=cylRes);
    }
}

